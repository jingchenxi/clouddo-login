package com.clouddo.clouddologin.controller;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.clouddo.clouddologin.domain.LoginDomain;
import com.clouddo.clouddologin.service.LoginService;


@RequestMapping(value = "/login")
@RestController
public class LoginController {
	
	private static Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	public LoginController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	LoginService loginService;
	
	@GetMapping("{username}")
	@CrossOrigin
	String get(@PathVariable("username") String username){
		logger.info(username);
		return loginService.get(username);
	}

	@RequestMapping("/list") 
	@Produces("application/json;charset=UTF-8")
	@CrossOrigin
	List list(@RequestParam(required=false) Map<String, Object> user) {
		List<LoginDomain> userlist = loginService.list(user);
		return userlist;
	}
	
	@RequestMapping(value = "/save")
	@Produces("application/json;charset=UTF-8")
	@CrossOrigin
    int save(@RequestBody(required = false) LoginDomain user) {
		return loginService.save(user);
	}
	
	@RequestMapping(value="/update")
	@Produces("application/json;charset=UTF-8")
	@CrossOrigin
	int update(@RequestBody LoginDomain user) {
		return loginService.update(user);
	}
	
	@RequestMapping(value="/delete")
	@Produces("application/json;charset=UTF-8")
	@CrossOrigin
	int remove(@RequestBody LoginDomain user) {
		return loginService.remove(user);
	}

}
