package com.clouddo.clouddologin.serviceimpl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clouddo.clouddologin.dao.LoginDao;
import com.clouddo.clouddologin.domain.LoginDomain;
import com.clouddo.clouddologin.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{
	
	private static Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	public LoginServiceImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	LoginDao loginDao;

	public List<LoginDomain> list(Map<String, Object> user) {
		// TODO Auto-generated method stub
		return loginDao.list(user);
	}

	public int save(LoginDomain user) {
		// TODO Auto-generated method stub
		int save = loginDao.save(user);
		return save;
	}

	public int update(LoginDomain user) {
		// TODO Auto-generated method stub
		int update = loginDao.update(user);
		return update;
	}

	public int remove(LoginDomain user) {
		// TODO Auto-generated method stub
		int remove = loginDao.remove(user.getUsername());
		return remove;
	}

	public String get(String username) {
		// TODO Auto-generated method stub
		logger.info("user = {} "+username);
		return loginDao.get(username);
	}


}
