package com.clouddo.clouddologin.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.clouddo.clouddologin.domain.LoginDomain;

@Mapper
public interface LoginDao {
	
	List<LoginDomain> list(Map<String, Object> user);

	int save(LoginDomain user);

	int update(LoginDomain user);

	int remove(String username);

	String get(String username);

}
