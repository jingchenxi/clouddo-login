package com.clouddo.clouddologin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableFeignClients(basePackages = {"com.clouddo"})
@EnableCaching
@EnableScheduling
@SpringBootApplication
public class LoginApplication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(LoginApplication.class,args);
		System.out.println("运行成功！！！！");
	}

}
