package com.clouddo.clouddologin.service;

import java.util.List;
import java.util.Map;

import com.clouddo.clouddologin.domain.LoginDomain;

public interface LoginService {

	List<LoginDomain> list(Map<String, Object> user);

	int save(LoginDomain user);

	int update(LoginDomain user);

	int remove(LoginDomain user);

	String get(String username);
	
}
