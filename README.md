# clouddo-login

#### 介绍
后台用spring cloud，基于 https://gitee.com/lcg0124/clouddo 李春光大神的后台进行服务的增加，来实现登录和注册。前后台分离。

#### 软件架构

首先启动clouddo-server，然后运行clouddo-login即可
server的端口号是8010，login的端口号是8008

#### 安装教程



``` java
git cloue https://gitee.com/jingchenxi/clouddo-login.git
```

``` js
eclipse 或者 IDEA
鼠标右键 --> import --> 导入进来
先启动clouddo-server，然后启动clouddo-login
```



#### 使用说明

1. 取之开源，用之开源
2. 点个star
3. 完了

#### 参与贡献

1. 白念寒
2. jingchenxi
