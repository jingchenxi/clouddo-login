/*
Navicat MySQL Data Transfer

Source Server         : 服务器
Source Server Version : 50529
Source Host           : 10.10.10.159:3306
Source Database       : clouddo

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2019-01-17 18:16:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `phonenumber` varchar(255) DEFAULT NULL COMMENT '手机号码',
  `wechat` varchar(255) DEFAULT NULL COMMENT '微信号绑定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('2', 'admin', '456', '123456789@163.com', '15548214562', 'JLfjdskf123');
INSERT INTO `login` VALUES ('3', 'hellosjk', 'hefjk', null, '45678945123', 'JLljksjfsk456');
INSERT INTO `login` VALUES ('4', 'hel', '123', '456433123@163.com', '45678945123', 'JLljksjfsk456');
