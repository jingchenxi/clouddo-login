import { Menu, Icon , Tooltip,Layout} from 'antd';
import React,{Component} from 'react';
import Cardd from './Cardd';
import {Link} from 'react-router-dom';
require ('antd/dist/antd.css');
require ('./Index.css');
const {Header, Content} = Layout;

const SubMenu = Menu.SubMenu;
const text = <span>返回主页</span>;
class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            current: 'mail',
            action:'',
        }
    };
    handleClick = (e) => {
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      }
    componentDidMount(){

    }
    render(){

        var obj;
        if(this.state.action===''){
            obj=<Cardd/>
        }
        return(
              <Layout>
               <Header className="top">
                 <Menu
                    onClick={this.handleClick}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"
                    theme="dark"
                    style={{width:'108%',marginLeft:'-4%'}}
                  >
                  <Menu.Item key="lz" className="workbench" style={{marginLeft:'10%'}}>
                    学习管理平台
                  </Menu.Item>
                  <Menu.Item key="workbench" className="application">
                    <Icon type="bank"  />工作台
                  </Menu.Item>
                  <Menu.Item key="application" className="application">
                    <Icon type="qrcode"  />应用
                  </Menu.Item>
                  <Menu.Item key="home" className="application" style={{marginLeft: '28%'}}>
                    <Tooltip placement="bottomRight"  title={text}>
                      <Link to="/Login">
                          <Icon type="bank"  style={{ fontSize: 16 }}/>
                      </Link>
                   </Tooltip>
                  </Menu.Item>
                  <SubMenu  title={<span className="submenu-title-wrapper"><Icon type="setting" /></span>}>
                    <Menu.Item key="setting:1">
                      <Link to="/resetPassword">
                        <Icon type="unlock" />修改密码
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="setting:2">
                      <Link to="/">
                        <Icon type="bank" />用户签退
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="setting:3"><Icon type="bars" />菜单文件</Menu.Item>
                  </SubMenu>
                 </Menu>
               </Header>
                 <Content style={{height:'600px'}}>
                   {
                     obj
                   }
                 </Content>
             </Layout>
        )
    }
}
export default Index;
