import React,{Component} from "react";
import {Link} from 'react-router-dom';
import { Input, Icon,Button,Form, } from 'antd';
import 'antd/dist/antd.css';
import './Reset.css';
const FormItem = Form.Item;

class Reset extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    };
    check = () => {
        this.props.form.validateFields(
          (err) => {
            if (!err) {
              console.info('success');
            }
          },
        );
      }
ComponentDidMount(){

}
render(){
    const { getFieldDecorator } = this.props.form;
    return(
         <div className="big">
          <Form onSubmit={this.handleSubmit} className="login-form">
             <div className="font">重 置 密 码</div>
          <FormItem>
            {getFieldDecorator('userName', {
              rules: [{ required: true, message: '请输入用户名!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户名" />
            )}
          </FormItem>
          <FormItem style={{height:'40px'}}>
            {getFieldDecorator('e-mail', {
              rules: [{ required: true, message: '请输入邮箱!' }],
            })(
              <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)'}} />} placeholder="注册的邮箱名" />
              
            )}
            <div className="youxiang">验证码会发送至您的注册邮箱</div>
          </FormItem>
          <FormItem>
            {getFieldDecorator('verification', {
              rules: [{ required: true, message: '请输入验证码!' }],
            })(
              <Input prefix={<Icon type="check-circle"  style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="验证码" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: '请输入密码!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('confirm', {
              rules: [{ required: true, message: '请输入密码!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="confirm" placeholder="确认密码" />
            )}
          </FormItem>
          <div className="cancel">
          <div className="change">
            <Button type="primary" htmlType="submit" onClick={this.check}>修改密码</Button>
          </div>
          <div className="change1">
          <Link to="/Login">
            <Button type="primary" htmlType="submit" >取消</Button>
          </Link>
          </div>
          </div>
          
         
          </Form>
         </div>
    )
  }
};
const WrappedNormalLoginForm = Form.create()(Reset);
export default WrappedNormalLoginForm;
// export default Reset;