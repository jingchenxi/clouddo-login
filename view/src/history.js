/* 这个history组件用来在ajax发送请求成功后进行方法内部跳转路由用 */

import createHistory from 'history/createBrowserHistory';

export default createHistory();