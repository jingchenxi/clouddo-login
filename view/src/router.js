import React, {Component} from 'react';
import Login from './Login';
import Index from './main/Index';
import Reset from './Reset/Reset.js';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import history from './history';

class router extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    };
    componentDidMount(){

    }
    render(){
        return(
        <Router history={history}>
        <div>
          <Switch>
            <Route path="/" exact component={Login}/>
            <Route path="/resetPassword" exact component={Reset}/>
            <Route path="/Login" exact component={Index}/>
            <Route path="/Callback" exact component={Index}/>
            <Route path="/Icons" exact component={Index}/>
          </Switch>
        </div>
      </Router>
        )
    };
}
export default router;
