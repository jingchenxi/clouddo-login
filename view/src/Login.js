import React,{Component} from 'react';
import $ from 'jquery';
import {Link} from 'react-router-dom';
import CanvasDotePage from './CanvasDotePage.js';
import { Input, Icon,Select,Button,Form,Checkbox, } from 'antd';
import history from './history';
const FormItem = Form.Item;
const Option = Select.Option;
require ('antd/dist/antd.css');
require('./Login.css');
class Login extends Component{
    constructor(props) {
      super(props);
      this.state = {
        username: '',
        password: '',
      }
    };
    check = () => {
      this.props.form.validateFields(
        (err) => {
          if (!err) {
            console.info('success');
          }
        },
      );
      var username = this.state.username;
      var password = this.state.password;
      console.log("username",username);
      console.log("password",password);
      
      $.ajax({
            url: 'http://localhost:8008/login/'+username,
            dataType: 'json',
            method: 'GET',
            success: function(data) {
              console.log("data",data);
              if(data == password){
                console.log("登录成功!!!");
                history.push("/Login");
                history.go(0);
              }else if(data != password){

                alert("登录失败!!");
                      
              }
            },
              error: function(xhr) {
              }
        })
    }
    componentDidMount(){

    }


    handleUsername=(e)=>{
      this.state.username=e.target.value;
      this.setState();
    }

    handlePassword=(e)=>{
      this.state.password=e.target.value;
      this.setState();
    }
    render(){
      const { getFieldDecorator } = this.props.form;
        return(
          <div className="style">
                <CanvasDotePage/>
          
          <div className="Login">
            
          <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: '请输入用户名!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户名" onChange={this.handleUsername}  value={this.state.username}/>
            )}
          </FormItem>

          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: '请输入密码!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" onChange={this.handlePassword}  value={this.state.password}/>
            )}
          </FormItem>
          <div>
          <Select defaultValue="游客" style={{ width: 300 }} >
            <Option value="jack">个人用户</Option>
          </Select>
        </div>
          <FormItem>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox>记住密码</Checkbox>
            )}
            <Link to="/resetPassword">
            <a className="login-form-forgot" href="http://">忘记密码</a>
            </Link>


            </FormItem>
            <FormItem>

            <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.check}>
              登录
            </Button>
            <Link to="/Login">
            <Button type="primary" htmlType="submit" className="login-form-button2" >
            快速登录
            </Button>
            </Link>
          </FormItem>
        </Form>
        </div>
        </div>
        )
    }
};


const WrappedNormalLoginForm = Form.create()(Login);
export default WrappedNormalLoginForm;
